import 'dart:io';

void main(List<String> args) {
  String? words = stdin.readLineSync()!;
  words.toLowerCase();
  List<String> wordLowerCaseList = words.split(' ');

  var map = {};

  for (var word in wordLowerCaseList) {
    if (!map.containsKey(word)) {
      map[word] = 1;
    } else {
      map[word] += 1;
    }
  }

  print(map);
}
